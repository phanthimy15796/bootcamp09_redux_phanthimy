import { render } from "@testing-library/react";

const initialState = {
  cart: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "SELECTED_PRODUCT":
      var cloneCart = [...state.cart];
      var foundIndex = cloneCart.findIndex((item) => {
        return item.product.id === action.payload.prod.id;
      });
      if (foundIndex === -1) {
        const cartItem = { product: action.payload.prod, quantity: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[foundIndex].quantity++;
      }
      state.cart = cloneCart;

      return { ...state };
    case "INCREASE_QUANTITY":
      var cloneCart = [...state.cart];
      var foundIndex = cloneCart.findIndex((item) => {
        return item.product.id === action.payload;
      });
      cloneCart[foundIndex].quantity++;
      state.cart = cloneCart;
      return { ...state };
    case "DICREASE_QUANTITY":
      var cloneCart = [...state.cart];
      var foundIndex = cloneCart.findIndex((item) => {
        return item.product.id === action.payload;
      });
      if (cloneCart[foundIndex].quantity <= 1) {
        cloneCart[foundIndex].quantity = 1;
      } else {
        cloneCart[foundIndex].quantity--;
      }
      state.cart = cloneCart;
      return { ...state };
    case "DELETE_PRODUCT":
      var cloneCart = [...state.cart];
      var foundIndex = cloneCart.findIndex((item) => {
        return item.product.id === action.payload;
      });
      cloneCart.splice(foundIndex, 1);
      state.cart = cloneCart;
      return { ...state };
    case "MAKE_PAYMENT":
      state.cart = [];
      return { ...state };
    default:
      return state;
  }
};
export default reducer;
