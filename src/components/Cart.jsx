import React, { Component } from "react";
import { connect } from "react-redux";
class Cart extends Component {
  increaseQuantity = (id)=>()=>{
    this.props.dispatch({
      type:'INCREASE_QUANTITY',
      payload:id,
    });
  };
  dicreaseQuantity =(id)=>()=>{
    this.props.dispatch({
      type:'DICREASE_QUANTITY',
      payload:id,
    });
  };
  deleteProduct = (id)=>()=>{
    this.props.dispatch({
      type:'DELETE_PRODUCT',
      payload:id,
    });
  };

  makePayment =()=>{
    this.props.dispatch({
      type:'MAKE_PAYMENT',
      
    });
  };
  renderCart = () => {
    return this.props.cart.map((item) => {
      const{id,name,img,price}=item.product;
      return (
        <tr key={id}>
          <td>{id}</td>
          <td>{name}</td>
          <td>
            <img src={img} height={280} width={250} alt="hinh anh" />
          </td>
          <td>
            <button onClick={this.increaseQuantity(id)} className="btn btn-danger">+</button>
            <span>{item.quantity}</span>
            <button onClick={this.dicreaseQuantity(id)} className="btn btn-danger">-</button>
          </td>
          <td>{price}</td>
          <td>{item.quantity*price}</td>
          <td>
            <button onClick={this.deleteProduct(id)} className="btn btn-danger">Delete</button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div
        className="modal fade"
        id="modelId"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-xl" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Modal title</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body ">
              <table className="table">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Name sp</th>
                    <th>Picture</th>
                    <th>Quanlity</th>
                    <th>Prices</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  {this.renderCart()}
                </tbody>
              </table>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button onClick={this.makePayment} type="button" className="btn btn-primary">
                PayMent
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    cart: state.cart.cart,
  };
};
export default connect(mapStateToProps)(Cart);
