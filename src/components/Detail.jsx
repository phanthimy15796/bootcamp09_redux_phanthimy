import React, { Component } from "react";
import { connect } from "react-redux";
class Detail extends Component {
  renderDetail = () => {
    return this.props.productList
      .filter((item) => {
        return item.id === this.props.selectedShowDetail;
      })
      .map((item) => {
        return (
          <div className="row" key={item.id}>
            <div className="col-5">
              <h4>{item.name}</h4>
              <img className="img-fluid" src={item.img} alt="product" />
            </div>
            <div className="col-7">
              <h5>Thong So Ki Thuat</h5>
              <table className="table">
                <tbody>
                  <tr>
                    <td>Screen</td>
                    <td>{item.screen}</td>
                  </tr>
                  <tr>
                    <td>FrontCamera</td>
                    <td>{item.frontCamera}</td>
                  </tr>
                  <tr>
                    <td>BackCamera</td>
                    <td>{item.backCamera}</td>
                  </tr>
                  <tr>
                    <td>Desc</td>
                    <td>{item.desc}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        );
      });
  };
  render() {
    return (
      <div className="container mt-5">
        {this.props.productList ? this.renderDetail() :""}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    productList: state.product.productList,
    selectedShowDetail: state.product.selectedShowDetail,
  };
};
export default connect(mapStateToProps)(Detail);
