import React, { Component } from "react";
import { connect } from "react-redux";
 class ProductItem extends Component {
   handleShowDetail =(id)=>()=>{
     this.props.dispatch({
       type:"SET_SHOW_DETAIL",
       payload:id,
     });
   };
  
   handleSelectedProd =(prod)=>{
    this.props.dispatch({
      type:'SELECTED_PRODUCT',
      payload :{
        prod,
      },
    });
   };
  render() {
    const{img,name,id}=this.props.prod
    return (
      <div className="card">
        <img className="" height={250} width="100%" src={img} alt />
        <div className="card-body">
          <h4 className="card-title">{name}</h4>
          <button onClick={this.handleShowDetail(id)} className='btn btn-success mr-2'>Detail</button>
          <button onClick={()=> this.handleSelectedProd(this.props.prod)} className='btn btn-secondary'>Cart</button>
        </div>
      </div>
    );
  };
};
const mapStateToProps = (state) => {
  return {
    productList: state.product.productList,
   
  };
};
export default connect (mapStateToProps)(ProductItem);
