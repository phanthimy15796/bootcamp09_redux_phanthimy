import React, { Component } from 'react'
import ProductList from './ProductList'
import Detail from './Detail'
import Cart from './Cart'
export default class Index extends Component {
    render() {
        return (
            <div>
                <h1 className="text-center my-4">Bài Tập Giỏ Hàng</h1>
                <h2 className="text-center mb-3" data-toggle="modal" data-target="#modelId">CART</h2>
                <ProductList/>
                <Detail/>
                <Cart/>
            </div>
        )
    }
}
