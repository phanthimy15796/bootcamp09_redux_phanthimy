import React, { Component } from "react";
import ProductItem from "./ProductItem";

import { connect } from "react-redux";
 class ProductList extends Component {
  renderProducList = () => {
    return this.props.productList.map((item) => {
      return (
        <div className="col-md-3" key={item.id}>
          <ProductItem prod={item}/>
        </div>
      );
    });
  };
  render() {
    return (
      <div className="container">
        <div className="row">{this.renderProducList()}</div>
      </div>
    );
  };
};
const mapStateToProps = (state) =>{
    return{
        productList: state.product.productList,
    };
};
export default connect(mapStateToProps)(ProductList);